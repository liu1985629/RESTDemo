package com.telenav.business;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.apache.amber.oauth2.common.OAuth;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@Path("/book/{bookname}")
public class Book {
	class BookObj {
		public String bookname;
		public double price;
		public String publisher;

		public BookObj(String bookname, double price, String publisher) {
			this.bookname = bookname;
			this.price = price;
			this.publisher = publisher;
		}
	}

	static ArrayList<BookObj> books = new ArrayList<BookObj>();

	@GET
	@Produces("application/xml")
	public String getBook(@PathParam("bookname") String bookname) {
		StringBuffer sb = new StringBuffer();
		sb.append("<books>");
		if (bookname.equals("all")) {
			for (BookObj b : books) {
				sb.append("<book>");
				sb.append("<name>RESTful Java Web Services_" + b.bookname + "</name>");
				sb.append("<publicationDate>December 2, 2009</publicationDate>");
				sb.append("<ISBN-10>1847196462</ISBN-10>");
				sb.append("<ISBN-13>978-1847196460</ISBN-13>");
				sb.append("<price>" + b.price + "</price>");
				sb.append("<publisher>" + b.publisher + "</publisher>");
				sb.append("</book>");
			}
		} else {
			for (BookObj b : books) {
				if (b.bookname.equals(bookname)) {
					sb.append("<book>");
					sb.append("<name>RESTful Java Web Services_" + b.bookname + "</name>");
					sb.append("<publicationDate>December 2, 2009</publicationDate>");
					sb.append("<ISBN-10>1847196462</ISBN-10>");
					sb.append("<ISBN-13>978-1847196460</ISBN-13>");
					sb.append("<price>" + b.price + "</price>");
					sb.append("<publisher>" + b.publisher + "</publisher>");
					sb.append("</book>");
				}
			}
		}
		sb.append("</books>");
		return sb.toString();

	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public String addBook(@FormParam("bookname") String bookname, @FormParam("price") double price, @Context HttpServletRequest request) {
		BookObj b = new BookObj(bookname, price, request.getAttribute(OAuth.OAUTH_CLIENT_ID).toString());
		books.add(b);
		StringBuffer sb = new StringBuffer();
		sb.append("<books>");
		sb.append("<book>");
		sb.append("<name>RESTful Java Web Services_" + b.bookname + "</name>");
		sb.append("<publicationDate>December 2, 2009</publicationDate>");
		sb.append("<ISBN-10>1847196462</ISBN-10>");
		sb.append("<ISBN-13>978-1847196460</ISBN-13>");
		sb.append("<price>" + b.price + "</price>");
		sb.append("<publisher>" + request.getAttribute(OAuth.OAUTH_CLIENT_ID).toString() + "</publisher>");
		sb.append("</book>");
		sb.append("</books>");
		return sb.toString();
	}

	@POST
	@Consumes("application/json")
	public String addBookJSON(String requestjson, @Context HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		try {
			JSONObject obj = new JSONObject(requestjson);
			BookObj b = new BookObj(obj.getString("bookname"), obj.getDouble("price"), request.getAttribute(OAuth.OAUTH_CLIENT_ID).toString());
			books.add(b);
			sb.append("<books>");
			sb.append("<book>");
			sb.append("<name>RESTful Java Web Services_" + b.bookname + "</name>");
			sb.append("<publicationDate>December 2, 2009</publicationDate>");
			sb.append("<ISBN-10>1847196462</ISBN-10>");
			sb.append("<ISBN-13>978-1847196460</ISBN-13>");
			sb.append("<price>" + b.price + "</price>");
			sb.append("<publisher>" + request.getAttribute(OAuth.OAUTH_CLIENT_ID).toString() + "</publisher>");
			sb.append("</book>");
			sb.append("</books>");
		} catch (JSONException e) {
			e.printStackTrace();
			sb.append(e.getMessage());
		}
		return sb.toString();
	}

	@PUT
	@Consumes("application/x-www-form-urlencoded")
	public String updateBook(@PathParam("bookname") String bookname, @FormParam("price") double price) {
		StringBuffer sb = new StringBuffer();
		sb.append("<books>");
		for (BookObj b : books) {
			if (b.bookname.equals(bookname)) {
				b.price = price;
				sb.append("<book>");
				sb.append("<name>RESTful Java Web Services_" + b.bookname + "</name>");
				sb.append("<publicationDate>December 2, 2009</publicationDate>");
				sb.append("<ISBN-10>1847196462</ISBN-10>");
				sb.append("<ISBN-13>978-1847196460</ISBN-13>");
				sb.append("<price>" + b.price + "</price>");
				sb.append("<publisher>" + b.publisher + "</publisher>");
				sb.append("</book>");
			}
		}
		sb.append("</books>");
		return sb.toString();
	}

	@DELETE
	public String delBook(@PathParam("bookname") String bookname) {
		StringBuffer sb = new StringBuffer();
		sb.append("<books>");

		Iterator<BookObj> it = books.iterator();
		while (it.hasNext()) {
			BookObj b = it.next();
			if (b.bookname.equals(bookname)) {
				it.remove();
			} else {
				sb.append("<book>");
				sb.append("<name>RESTful Java Web Services_" + b.bookname + "</name>");
				sb.append("<publicationDate>December 2, 2009</publicationDate>");
				sb.append("<ISBN-10>1847196462</ISBN-10>");
				sb.append("<ISBN-13>978-1847196460</ISBN-13>");
				sb.append("<price>" + b.price + "</price>");
				sb.append("<publisher>" + b.publisher + "</publisher>");
				sb.append("</book>");
			}
		}
		sb.append("</books>");
		return sb.toString();
	}
}
