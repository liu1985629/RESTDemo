package com.telenav.oauth;

import java.security.Principal;

import org.apache.amber.oauth2.rsfilter.OAuthClient;
import org.apache.amber.oauth2.rsfilter.OAuthDecision;

public class MyDecison implements OAuthDecision {
	private boolean isAuthorized = false;
	private Principal principal;
	private OAuthClient OAuthClient;

	@Override
	public boolean isAuthorized() {
		return isAuthorized;
	}

	@Override
	public Principal getPrincipal() {
		return principal;
	}

	@Override
	public OAuthClient getOAuthClient() {
		return OAuthClient;
	}

	public void setAuthorized(boolean isAuthorized) {
		this.isAuthorized = isAuthorized;
	}

	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}

	public void setOAuthClient(OAuthClient oAuthClient) {
		OAuthClient = oAuthClient;
	}

}
