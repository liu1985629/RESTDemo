package com.telenav.oauth;

import javax.servlet.http.HttpServletRequest;

import org.apache.amber.oauth2.common.error.OAuthError;
import org.apache.amber.oauth2.common.exception.OAuthProblemException;
import org.apache.amber.oauth2.rsfilter.OAuthClient;
import org.apache.amber.oauth2.rsfilter.OAuthDecision;
import org.apache.amber.oauth2.rsfilter.OAuthRSProvider;

public class MyRSProvider implements OAuthRSProvider {

	@Override
	public OAuthDecision validateRequest(String rsId, String token, HttpServletRequest req) throws OAuthProblemException {
		MyDecison myDecison = new MyDecison();
		if (token.equals("access_token_valid")) {
			myDecison.setAuthorized(true);
			myDecison.setPrincipal(null);
			myDecison.setOAuthClient(new OAuthClient() {

				@Override
				public String getClientId() {
					return "valid_client_id";
				}
			});
		} else if (token.equals("access_token_expired")) {
			throw OAuthProblemException.error(OAuthError.ResourceResponse.EXPIRED_TOKEN);
		} else if (token.equals("access_token_insufficient")) {
			throw OAuthProblemException.error(OAuthError.ResourceResponse.INSUFFICIENT_SCOPE);
		} else {
			throw OAuthProblemException.error(OAuthError.ResourceResponse.INVALID_TOKEN);
		}
		return myDecison;
	}

}
