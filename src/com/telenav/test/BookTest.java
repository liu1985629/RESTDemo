package com.telenav.test;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.codehaus.jettison.json.JSONObject;

public class BookTest {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod("http://127.0.0.1:8012/RESTDemo/jaxrs/book/add");
		RequestEntity reqEntity = new StringRequestEntity("bookname=aaa&price=77.0", "application/x-www-form-urlencoded", "utf-8");
		post.setRequestEntity(reqEntity);
		post.setRequestHeader("Authorization", "Bearer access_token_valid");
		client.executeMethod(post);
		System.out.println(post.getResponseBodyAsString());
		
		post = new PostMethod("http://127.0.0.1:8012/RESTDemo/jaxrs/book/add");
		reqEntity = new StringRequestEntity("bookname=bbb&price=88.0", "application/x-www-form-urlencoded", "utf-8");
		post.setRequestEntity(reqEntity);
		post.setRequestHeader("Authorization", "Bearer access_token_valid");
		client.executeMethod(post);
		System.out.println(post.getResponseBodyAsString());
		
		post = new PostMethod("http://127.0.0.1:8012/RESTDemo/jaxrs/book/add");
		JSONObject json=new JSONObject();
		json.put("bookname", "ccc");
		json.put("price", 88.0);
		reqEntity = new StringRequestEntity(json.toString(), "application/json", "utf-8");
		post.setRequestEntity(reqEntity);
		post.setRequestHeader("Authorization", "Bearer access_token_valid");
		client.executeMethod(post);
		System.out.println(post.getResponseBodyAsString());
		
		
		PutMethod put = new PutMethod("http://127.0.0.1:8012/RESTDemo/jaxrs/book/aaa");
		reqEntity = new StringRequestEntity("bookname=aaa&price=77.5", "application/x-www-form-urlencoded", "utf-8");
		put.setRequestEntity(reqEntity);
		put.setRequestHeader("Authorization", "Bearer access_token_valid");
		client.executeMethod(put);
		System.out.println(put.getResponseBodyAsString());
		
		GetMethod get=new GetMethod("http://127.0.0.1:8012/RESTDemo/jaxrs/book/all");
		get.setRequestHeader("Authorization", "Bearer access_token_valid");
		get.setRequestHeader("Authorization", "Bearer access_token_valid");
		client.executeMethod(get);
		System.out.println(get.getResponseBodyAsString());
		
		DeleteMethod del = new DeleteMethod("http://127.0.0.1:8012/RESTDemo/jaxrs/book/aaa");
		del.setRequestHeader("Authorization", "Bearer access_token_valid");
		client.executeMethod(del);
		System.out.println(del.getResponseBodyAsString());
		
		get=new GetMethod("http://127.0.0.1:8012/RESTDemo/jaxrs/book/all");
		get.setRequestHeader("Authorization", "Bearer access_token_valid");
		client.executeMethod(get);
		System.out.println(get.getResponseBodyAsString());
		

	}

}
