package com.telenav.test;

import org.apache.amber.oauth2.client.request.OAuthClientRequest;
import org.apache.amber.oauth2.common.OAuth;
import org.apache.amber.oauth2.common.message.types.ResponseType;
import org.apache.amber.oauth2.ext.dynamicreg.client.request.OAuthClientRegistrationRequest;
import org.apache.amber.oauth2.ext.dynamicreg.client.request.OAuthClientRegistrationRequest.OAuthRegistrationRequestBuilder;
import org.apache.amber.oauth2.ext.dynamicreg.common.OAuthRegistration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

public class OAuthTest {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		HttpClient client = new HttpClient();
		
		System.out.println("----success for get authorization code----");
		OAuthClientRequest request = OAuthClientRequest.authorizationLocation("http://127.0.0.1:8012/RESTDemo/jaxrs/authz").setClientId("valid_client_id")
				.setRedirectURI("valid_redirect_url").setScope("valid_scope").setResponseType(ResponseType.CODE.toString()).buildQueryMessage();
		GetMethod get_authorizationCode = new GetMethod(request.getLocationUri());
		get_authorizationCode.setFollowRedirects(false);
		client.executeMethod(get_authorizationCode);
		System.out.println(get_authorizationCode.getResponseHeader("Location").getValue());

		System.out.println("----fail for get authorization code----");
		request = OAuthClientRequest.authorizationLocation("http://127.0.0.1:8012/RESTDemo/jaxrs/authz").setClientId("invalid_client_id")
				.setRedirectURI("valid_redirect_url").setScope("valid_scope").setResponseType(ResponseType.CODE.toString()).buildQueryMessage();
		get_authorizationCode = new GetMethod(request.getLocationUri());
		get_authorizationCode.setFollowRedirects(false);
		client.executeMethod(get_authorizationCode);
		System.out.println(get_authorizationCode.getResponseHeader("Location").getValue());

		System.out.println("----success for get token----");
		request = OAuthClientRequest.authorizationLocation("http://127.0.0.1:8012/RESTDemo/jaxrs/authz").setClientId("valid_client_id")
				.setRedirectURI("valid_redirect_url").setParameter(OAuth.OAUTH_CODE, "valid_code").setScope("valid_scope")
				.setResponseType(ResponseType.TOKEN.toString()).buildQueryMessage();
		GetMethod get_token = new GetMethod(request.getLocationUri());
		get_token.setFollowRedirects(false);
		client.executeMethod(get_token);
		System.out.println(get_token.getResponseHeader("Location").getValue());

		System.out.println("----fail for get token----");
		request = OAuthClientRequest.authorizationLocation("http://127.0.0.1:8012/RESTDemo/jaxrs/authz").setClientId("valid_client_id")
				.setRedirectURI("valid_redirect_url").setParameter(OAuth.OAUTH_CODE, "invalid_code").setScope("valid_scope")
				.setResponseType(ResponseType.TOKEN.toString()).buildQueryMessage();
		get_token = new GetMethod(request.getLocationUri());
		get_token.setFollowRedirects(false);
		client.executeMethod(get_token);
		System.out.println(get_token.getResponseHeader("Location").getValue());		

		System.out.println("----success for register----");
		request = OAuthClientRegistrationRequest
	            .location("http://127.0.0.1:8012/RESTDemo/jaxrs/register", OAuthRegistration.Type.PUSH)
	            .setName("BOOK APP")
	            .setUrl("http://127.0.0.1:8012/RESTDemo")
	            .setDescription("You can add,update,deleate and query books.")
	            .setIcon("http://127.0.0.1:8012/RESTDemo/icon.png")
	            .setRedirectURL("valid_redirect_url")
	            .buildJSONMessage();
		PostMethod register = new PostMethod(request.getLocationUri());
		register.setRequestEntity(new StringRequestEntity(request.getBody(),"application/json","utf-8"));
		client.executeMethod(register);
		System.out.println(register.getResponseBodyAsString());
	}

}
